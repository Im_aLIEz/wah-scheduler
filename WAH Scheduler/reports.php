<?php
include('db_con.php');
function switchColor($rowValue) { 

    //Define the colors first 
    $color1 = '#d0f406'; 
    $color2 = '#ed5b4e'; 
    $color3 = '#5287f2';  
    switch ($rowValue) { 
        case 'P': 
            echo $color1; 
            break; 
        case 'C': 
            echo $color2; 
            break; 
        default: 
            echo $color3; 
    } 
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Reports </title>
		<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  		<link href="./clusterize.css" rel="stylesheet">
		<script src="./clusterize.min.js"></script>

  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style type="text/css">
body {
  margin: 0;
}

/* Style the header */
.header {
    background-color: #f2f2f2;
    padding: 20px;
    text-align: center;
    width: 100%;
    letter-spacing: 2px;
    text-indent: 50px;
    letter-spacing: 3px;
}

#reports {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#reports td, #reports th {
    border: 1px solid #ddd;
    padding: 8px;
    text-align: center;

}

#reports th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: center;
    background-color: #4CAF50;
    color: white;
}



</style>
</head>
<body>

<div class="header">
  <h1>WAH Training Reports</h1>
</div>

<div style="overflow-x:auto;">
    <table id ="reports">
	<tr>
	<th>Module Name</th>
	<th>Event Title</th>
    <th>Facility</th>
    <th>Start Date</th>
	<th>End Date</th>
    
    <th>Team</th>
    <th>Length</th>
    <th>Status</th>
    <th>Remarks</th>
	</tr>

	  <?php
	  # Query the data from database.
    	$sqlSelect = "SELECT tbl_schedule.*, tbl_module.module_name FROM tbl_schedule INNER JOIN tbl_module ON tbl_schedule.module_code = tbl_module.id  ORDER BY start_date";
      if($result = mysqli_query($connection, $sqlSelect))
        {
            while($row = mysqli_fetch_array($result))
            {
                ?>
                <tr class='tr' bgcolor="<?php switchColor($row['status'])?>">
                <?php
                $scheduleid = $row['id'];
                $sqlSelect2 = "SELECT * FROM tbl_staff_schedule where training_id = '$scheduleid'";
                $result1 = mysqli_query($connection, $sqlSelect2);
                echo "<td>" . $row['module_name'] . "</td>";
                echo "<td>" . $row['event_name'] . "</td>";
                echo "<td>" . $row['facility'] . "</td>";
                echo "<td>" . $row['start_date'] . "</td>";
                echo "<td>" . $row['end_date'] . "</td>";
                echo "<td>" ;
                while($row1 = mysqli_fetch_array($result1))
                {
               echo $row1['staff_name'] . ", ";
                }
                $sqlSelect3 = "SELECT length FROM tbl_staff_schedule WHERE training_id = '$scheduleid'";
                if($result2 = mysqli_query($connection, $sqlSelect3));
                $row2 = mysqli_fetch_array($result2);
                echo "<td>"; echo $row2['length']; echo "</td>";
                echo  "<td>". $row['status'] . "</td>";
                echo "<td>" . $row['remarks']. "</td>";
                echo "</td>";echo "</tr>";
            }   

        }
	  ?>


</div>
</body>
</html>