<?php include('maintenanceInsert.php'); 

	if(isset($_GET['edit'])){
		$id = $_GET['edit'];
		$edit_state = true;
		$rec = mysqli_query($db, "SELECT * FROM tbl_module WHERE id=$id");
		$record = mysqli_fetch_array($rec);
		$id = $record['id'];
		$name = $record['module_name'];
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Module Maintenance</title>
	<link rel="stylesheet" type="text/css" href="style.css">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
</head>
<body>
	<?php if (isset($_SESSION['msg'])): ?>
		<div>
			<?php
				echo $_SESSION['msg'];
				unset($_SESSION['msg']); 
			 ?>
		</div>
	<?php endif ?>
	<div class="container col-md-6">
		<table class="table table-bordered table-light">
		<thead>
			<tr>
				<th>Module</th>
				<th colspan="2"> Action </th>
			</tr>
		</thead>
		<tbody>
			<?php while($row = mysqli_fetch_array($results)){ ?>
				<tr>
					<td><?php echo $row['module_name']; ?></td>
					<td>
						<a type="button" class="edit_btn btn btn-primary" href="Maintenance.php?edit=<?php echo $row['id'];?>">Edit</a>
						<a type="button"  class="del_btn btn btn-danger" href="maintenanceInsert.php?del=<?php echo $row['id'];?>">Delete</a>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	</div>
	

	<form method ="post" action="maintenanceInsert.php">
	<input type="hidden" name="id" value="<?php echo $id; ?>">
		<div class="input-group">
			<label>Module Name</label>
			<input type="text" name="name" value="<?php echo $name;?>">
		</div>
		<div class="input-group">
			<?php if ($edit_state == false): ?>
				<button type="submit" name="save" class="btn btn-success">Save</button>
			<?php else: ?>
				<button type="submit" name="update" class="btn">Update</button>
			<?php endif ?>
		</div>
	</form>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

</body>
</html>